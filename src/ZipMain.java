import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ZipMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> pliki=new ArrayList<>();
//		nazwy plik�w nale�y dostosowa� do tego co chcemy zipowa� 
		pliki.add("1612.03281.pdf");
		pliki.add("6907133-dzone-guide-artificialintelligence-2017.pdf");
		pliki.add("7263880-dzone-guidetowebdev-2017.pdf");
		pliki.add("Android-UI-Design.pdf");
		pliki.add("Git-Tutorial.pdf");
		ArrayList<Future<ZipInfo>> listaWynikow=
				new ArrayList<>();
        ExecutorService pool=Executors.newFixedThreadPool(2);
        for(String fName: pliki){
        	listaWynikow.add(pool.submit(new ZipWorker(fName)));
        }
       pool.shutdown();
       try {
		if(pool.awaitTermination(10, TimeUnit.SECONDS)){
	     for(Future<ZipInfo> info: listaWynikow)
	     {
	    	 System.out.println("Plik "+info.get().getFileName()+
	    			 " zosta� zzipowany w "+info.get().getDuration()+
	    			 " milisekund i mia� rozmiar "+info.get().getSize()+
	    			 " bajt�w.");
	     }
		   }
		else{
			System.out.println("Zadania nie zosta�y zako�czone w zadanym czasie.");
		}
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ExecutionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	}

}
