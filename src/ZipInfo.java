
public class ZipInfo {
	private long duration;
    private int size;
    private String fileName;
	public ZipInfo(long duration, int size,String fileName) {
		super();
		this.duration = duration;
		this.size = size;
		this.fileName=fileName;
	}
	public String getFileName() {
		return fileName;
	}
	public long getDuration() {
		return duration;
	}
	public int getSize() {
		return size;
	}
    
}
