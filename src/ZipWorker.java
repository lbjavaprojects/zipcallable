import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipWorker implements Callable<ZipInfo> {
   private String filePath;
   public ZipWorker(String filePath){
	  this.filePath=filePath;   
   }
	@Override
	public ZipInfo call() throws Exception {
		long duration=0;
		int size=0;
		long timeStart=System.currentTimeMillis();
		try(FileInputStream fis=new FileInputStream(filePath);
				FileOutputStream fos=
						new FileOutputStream(dropExtention(extractFileName()));
				ZipOutputStream zos=new ZipOutputStream(fos)){
			ZipEntry ze=new ZipEntry(extractFileName());	
			zos.putNextEntry(ze);
			int buffsize=1024,c;
			byte[] bufor=new byte[buffsize];
			System.out.println("Zipuj� plik... "+extractFileName());
			while((c=fis.read(bufor, 0, buffsize))>-1){
				zos.write(bufor, 0, c);
				size+=c;
			}
			zos.closeEntry();
			}
		long timeStop=System.currentTimeMillis();
		return new ZipInfo(timeStop-timeStart,size,extractFileName());
	}
	
  private String extractFileName(){
	  return filePath.substring(filePath.lastIndexOf("/")+1);
  }
  
  private String dropExtention(String fileName){
	  return fileName.substring(0,fileName.lastIndexOf("."));
  }

}
